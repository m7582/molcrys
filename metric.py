import numpy as np
from ase import data
import itertools


ran = [-1, 0, 1]
neighbours = list(itertools.product(ran, ran, ran))
print(neighbours)

def nb_vector(ijk, cell):
    ijk = np.array(ijk)
    return np.dot(cell.T, ijk)


def nb_array(cell):
    nb_arr = np.zeros((3, 27))
    for I, ijk in enumerate(neighbours):
        nb_arr[:, I] = nb_vector(ijk, cell)
    return nb_arr


def _scale_matrix(atoms1, atoms2, norm):
    if norm == 'cov':
        cov_rads1 = data.covalent_radii[atoms1.get_atomic_numbers()]
        cov_rads2 = data.covalent_radii[atoms1.get_atomic_numbers()]
        return np.add.outer(cov_rads1, cov_rads2)

    elif norm == 'vdW':
        vdw_rads1 = data.vdw_radii[atoms1.get_atomic_numbers()]
        vdw_rads2 = data.vdw_radii[atoms1.get_atomic_numbers()]
        return np.add.outer(vdw_rads1, vdw_rads2)





def distance_matrix_new(atoms1, atoms2, norm='cov', wrapping=True):
    """ Provides separations between all atoms

    Args:
        param1 : ASE (periodic) atoms object
        param1 : ASE (periodic) atoms object
        param2 (optional) : by default separations between atoms
                            are scaled by covelent radii.
                            Other options are "vdW" and False (i.e. ang).
        param3 (optional) : whether wrapping is considered, distances to 
                            neighboring cells are considered. Default is True.

    Returns:
        N1xN2 array -- distance matrix """

    pos1 = atoms1.positions
    pos2 = atoms2.positions

    if norm:
        scale = _scale_matrix(atoms1, atoms2, norm)
    else:
        scale = 1

    if wrapping:
        N1 = len(atoms1)
        N2 = len(atoms2)
        xdiffs = np.zeros((N1, N2, 27))
        ydiffs = np.zeros((N1, N2, 27))
        zdiffs = np.zeros((N1, N2, 27))
        for step, ijk in enumerate(neighbours):
            ijk = np.array(ijk)
            sep_image = np.dot(atoms1.cell.T, ijk)
            xdiffs[:,:,step] = np.subtract.outer(pos1[:, 0], pos2[:, 0] + sep_image[0])
            ydiffs[:,:,step]  = np.subtract.outer(pos1[:, 1], pos2[:, 1] + sep_image[1])
            zdiffs[:,:,step]  = np.subtract.outer(pos1[:, 2], pos2[:, 2] + sep_image[2])
        #xmin2 = np.min(xdiffs**2, 2)
        #ymin2 = np.min(ydiffs**2, 2)
        #zmin2 = np.min(zdiffs**2, 2)

        distances = (xdiffs**2 + ydiffs**2 + zdiffs**2)**0.5
        diff = np.min(distances, 2)
        return diff / np.transpose(scale)
    else:
        diffs = np.subtract.outer(pos1[:, 0], pos2[:, 0])**2
        diffs += np.subtract.outer(pos1[:, 1], pos2[:, 1])**2
        diffs += np.subtract.outer(pos1[:, 2], pos2[:, 2])**2
        distances = (diffs)**(0.5)
        return distances / scale


def distance_matrix(atoms1, atoms2, norm='cov', wrapping=True):
    """ Provides separations between all atoms

    Args:
        param1 : ASE (periodic) atoms object
        param1 : ASE (periodic) atoms object
        param2 (optional) : by default separations between atoms
                            are scaled by covelent radii.
                            Other options are "vdW" and False (i.e. ang).
        param3 (optional) : whether wrapping is considered, distances to 
                            neighboring cells are considered. Default is True.

    Returns:
        N1xN2 array -- distance matrix """

    pos1 = atoms1.positions
    pos2 = atoms2.positions

    if norm:
        scale = _scale_matrix(atoms1, atoms2, norm)
    else:
        scale = 1

    N1 = len(atoms1)
    N2 = len(atoms2)
    distances = np.zeros((N1, N2, 27))
    if wrapping:
        for step, ijk in enumerate(neighbours):
            ijk = np.array(ijk)
            sep_image = np.dot(atoms1.cell.T, ijk)
            diffs = np.subtract.outer(pos1[:, 0], pos2[:, 0] + sep_image[0])**2
            diffs += np.subtract.outer(pos1[:, 1],
                                       pos2[:, 1] + sep_image[1])**2
            diffs += np.subtract.outer(pos1[:, 2],
                                       pos2[:, 2] + sep_image[2])**2
            distances[:, :, step] = (diffs)**(0.5)
        return np.min(distances, 2) / scale
    else:
        diffs = np.subtract.outer(pos1[:, 0], pos2[:, 0])**2
        diffs += np.subtract.outer(pos1[:, 1], pos2[:, 1])**2
        diffs += np.subtract.outer(pos1[:, 2], pos2[:, 2])**2
        distances = (diffs)**(0.5)
        return distances / scale


def adjacency_matrix(atoms, cutoff, diag=1, **kwargs): 
    """" Return adjacancy matrix for an atoms object
         based on distances matrix.

    Args:
        param1 : ASE (periodic) atoms object
        param2 : cutoff  
        diag (optinal) : value on diagonal, default 1
        **kwargs: as in separation matrix

    Returns:
        N1xN2 array -- adjancancy matrix """

    dist = distance_matrix_new(atoms, atoms, **kwargs)
    adj_mat = (dist < cutoff)*1
    if not diag==1:
       np.fill_diagonal(adj_mat, diag)
    return adj_mat
    

def closest_neighbor_vector(T, cell):
    '''
    Given a vector T, which vector between neighboring
    unit cell is it most similar to?

    Args:
        param1: vector (3x1 array)
        param2: unic cell (3x3 array)

    Return:
        3x1 array

    '''
    nbs = nb_array(cell)
    i = np.argmin(np.linalg.norm(nbs - T[:,np.newaxis], axis= 0))
    return nbs[:, i]


def atoms_distance(atoms1, atoms2, atom_types = True, wrapping=True):
    """
    Note: Current version assumes same size of objects. 
    
    Args:
        param1: atoms object
        param2: atoms object

    Return:
        A measure of the Carteisan distance between two atoms objects
    """
    
    if atom_types:
        chem1 = atoms1.get_chemical_symbols()
        chem2 = atoms2.get_chemical_symbols()
        if list(np.unique(chem1)) != list(np.unique(chem2)):
            print('atom types must be the same')
            exit()
        
        dist = 0
        for symb in np.unique(chem1):
            mask1 = np.array(chem1) == symb
            mask2 = np.array(chem2) == symb
            dist_mat = distance_matrix(atoms1[mask1], atoms2[mask2])
            dist += np.sum(np.min(dist_mat, 0))

        return dist/len(atoms1) # check this normalization

    else:
        dist = distance_matrix(atoms1, atoms2, norm=False, wrapping=wrapping)
        return np.mean(np.min(dist, 0))

    

def num_hyd_bonds_dir(atoms1, atoms2, cutoff_NH=2.2, cutoff_OH=2.4, cutoff_NF = 2.5, wrapping=True):
    """
    Args:
        param1: atoms object
        param2: atoms object

    Return:
        A measure of the shortest Carteisan distance between two atoms objects
    """
    
    chem1 = atoms1.get_chemical_symbols()
    chem2 = atoms2.get_chemical_symbols()
    dists = []
    
    mask1 = np.array(chem1) == 'H'
    maskO = np.array(chem2) == 'O'
    maskN = np.array(chem2) == 'N'
    maskF = np.array(chem2) == 'F'
    dist_mat_HO = distance_matrix(atoms1[mask1], atoms2[maskO], norm=False)
    dist_mat_HN = distance_matrix(atoms1[mask1], atoms2[maskN], norm=False)
    dist_mat_HF = distance_matrix(atoms1[mask1], atoms2[maskF], norm=False)
    N_O = np.sum((dist_mat_HO < cutoff_OH)*1.0)
    N_N = np.sum((dist_mat_HN < cutoff_NH)*1.0)
    N_F = np.sum((dist_mat_HF < cutoff_NF)*1.0)
    return int(N_O + N_N + N_F)

    
def num_hyd_bonds(atoms1, atoms2, **kwargs):
    return num_hyd_bonds_dir(atoms1, atoms2, **kwargs) + num_hyd_bonds_dir(atoms2, atoms1, **kwargs)






