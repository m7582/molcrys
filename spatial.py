from scipy.spatial import ConvexHull
import miniball
from math import pi, sqrt
import networkx as nx
import moltools as mt


def sphericity(molecule, core=True, return_components=False):
    """
    Calculate the sphericity of a molecule.

    Parameters:
    -----------
    molecule: object
        The molecule object.
    core: bool, optional (default=True)
        If True, only the core part of the molecule will be considered for sphericity calculation.
    return_components: bool, optional (default=False)
        If True, returns the components of the sphericity calculation: volumes of the convex hull
        and the bounding sphere.

    Returns:
    --------
    float or tuple:
        The sphericity value, or a tuple containing the volumes of the convex hull and the bounding
        sphere if return_components is True.
    """

    if not mt.is_connected(molecule, wrapping=False):
        raise ValueError("Error: molecule is not connected without wrapping")

    def calculate_sphericity(coords):
        """
        Helper function to calculate sphericity from the input coordinates.

        Parameters:
        -----------
        coords: array_like
            The coordinates of the atoms in the molecule.

        Returns:
        --------
        tuple:
            The volumes of the convex hull and the bounding sphere.
        """
        CH = ConvexHull(coords)
        V_conv = CH.volume
        C, r2 = miniball.get_bounding_ball(coords)
        V_ball = 4 * pi * sqrt(r2) ** 3 / 3
        return V_conv, V_ball

    if core:
        adj_mat = mt.adjacency_matrix(molecule, cutoff=1.1, diag=0)
        G = nx.from_numpy_matrix(adj_mat)
        bridge = nx.bridges(G)
        G.remove_edges_from(bridge)
        G.remove_nodes_from(list(nx.isolates(G)))

        if len(list(G)) > 3:
            atoms = molecule[list(G)]
            mols = mt.decompose(atoms)

            V_convs = []
            V_balls = []
            for mol in mols:
                Rs = mol.positions
                V_conv, V_ball = calculate_sphericity(Rs)
                V_convs.append(V_conv)
                V_balls.append(V_ball)

            if return_components:
                return V_convs, V_balls

            return max(V_conv / V_ball for V_conv, V_ball in zip(V_convs, V_balls))

    else:
        Rs = molecule.positions
        V_conv, V_ball = calculate_sphericity(Rs)
        if return_components:
            return V_conv, V_ball

        return V_conv / V_ball

