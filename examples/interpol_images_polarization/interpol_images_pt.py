#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
An example to produce interpolated images needed to calculate spontaneous 
polarizations in proton-transfer ferroelectrics.
starting from the ferroelectric phase gradually mixing the ferrolectric phase 
with the inverted phase:
    0.0 mixing = ferroelctric phase
    0.5 mixing = hypothetical symmetrized paraelectric phase 

"""

import numpy as np
from ase.io import read, write
from molcrys import transform
import spglib


atoms = read('REZBOP_vdwdf2_relaxed.cif')

num_images = 6
image_number = 0

for frac in np.linspace(0, 0.5, num_images):
    # Interpolate the atoms using a linear interpolation
    interpolated_atoms = transform.proton_transfer(atoms, alpha=frac)
    print('interpolated atoms')

    # The space group of the last image is centrosymmetric
    write(f'POSCAR_{image_number}', interpolated_atoms, format='vasp')
    image_number += 1
    
