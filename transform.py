import numpy as np
from molcrys.metric import distance_matrix
from molcrys import metric
from molcrys import aux
from ase import Atoms

def proton_transfer(atoms, alpha=0.5):
    """"  
    For a ferroelectric molecular crystal with psuodo-inversion 
    symmetry. Set up paralectric phase with alpha=0.5

    Args:
        param1 : ASE (periodic) atoms object
        param3 (optional) : degree of mixing alpha=0.5 

        Returns:
        ASE periodc atoms object. 
    """
    atoms_invert = atoms.copy()
    atoms_invert.positions = - atoms.positions
    T = aux.match_atoms(atoms, atoms_invert, exclude_atoms=['H'])
    atoms_invert.translate(T)
    atoms_new = interpolate_linear(atoms, atoms_invert, alpha)
    return atoms_new



def interpolate_linear(atoms1, atoms2, alpha=0.5):
    """" Linear atom-wise interpolation of two atoms objects. 
         Can be used to initalize NEB.
    Args:
        param1 : ASE (periodic) atoms object
        param2 : ASE (periodic) atoms object
        param3 (optional) : degree of mixing 

        Returns:
        ASE periodc atoms object. 
    """
    new_cell = atoms1.cell*(1 - alpha) + alpha*atoms2.cell
    
    atoms_merge = Atoms([], pbc=True)
    atoms_merge.set_cell(new_cell)

    if atoms1.get_chemical_formula() != atoms2.get_chemical_formula():
        print('''atoms object must consist of same elements. 
                Exiting....''')
        exit()

    chem1 = atoms1.get_chemical_symbols()
    chem2 = atoms2.get_chemical_symbols()
    
    for symb in np.unique(chem1):
        mask1 = np.array(chem1) == symb
        mask2 = np.array(chem2) == symb
        
        atoms1_elem = atoms1[mask1]
        atoms2_elem = atoms2[mask2]

        dist_matrix = distance_matrix(atoms1_elem, atoms2_elem)
        inds =  np.argmin(dist_matrix, 1)
        pos1 = atoms1_elem.positions
        pos2 = atoms2_elem.positions
        T_array = np.zeros((len(atoms1_elem), 3))
        for i in range(len(atoms1_elem)):
            diff = pos1[i] - pos2[inds][i]
            T_array[i] = metric.closest_neighbor_vector(diff, atoms2.cell)
            
        new_pos = pos1*(1 - alpha) + (pos2[inds] + T_array)*alpha 
        atoms_merge_elem = atoms1_elem.copy()
        atoms_merge_elem.positions = new_pos
        atoms_merge += atoms_merge_elem
    
    return atoms_merge

def invert_atoms(atoms):
    """"  
    A function to get the inverted atoms object by applying inversion operation
    and overlay the atomic species. The indices are the same in the atoms and 
    inverted atoms objects.

    Args:
        param1 : ASE (periodic) atoms object
        

        Returns:
        ASE periodc inverted atoms object. 
    """
    inverted_atoms = atoms.copy()
    inverted_atoms.positions = -atoms.positions
    T = aux.match_atoms(atoms, inverted_atoms, exclude_atoms=['H'])
    inverted_atoms.translate(T)
    inverted_atoms.wrap()

    merged_atoms = Atoms([], pbc=True)
    merged_atoms.set_cell(atoms.cell)

    for symb in np.unique(atoms.get_chemical_symbols()):
        mask = np.array(atoms.get_chemical_symbols()) == symb
        mask2 = np.array(inverted_atoms.get_chemical_symbols()) == symb

        sub_atoms = atoms[mask]
        sub_atoms2 = inverted_atoms[mask2]

        dist_matrix = metric.distance_matrix(sub_atoms, sub_atoms2)
        inds = np.argmin(dist_matrix, 1)
        # Reorder the atoms in the inverted atoms object to match the order of the atoms in the original atoms object
        sub_atoms2.positions[np.arange(len(inds))] = sub_atoms2.positions[inds]
        merged_atoms += sub_atoms2

    return merged_atoms

