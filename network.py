import networkx as nx
import numpy as np
import moltools
from moltools import aux
from moltools import adjacency_matrix

def show():
    import matplotlib.pyplot as plt
    plt.show()



def atoms2network(atoms, cutoff):
    """Make atoms object into a nework   """
    adj_mat = adjacency_matrix(atoms, cutoff, diag=0)
    return nx.from_numpy_matrix(adj_mat)


def has_lone_tetahedra(atoms, exclude_atoms=['H'], cutoff=1.1):
    """ Check if a crystal structure has lone tehtradra structure, example IO4.  

    Args:
        param1 : ASE periodic atoms object, a molecule or
                molecular crystal.
        param2 (optional): List of atom types (chemical symbols)
                to exclude in the network analysis. By default 'H' (hydrogen)
                is excluded
        param3 (optional): cutoff used to define covalent networks

    Returns:
        True / False (Bool)
    """
    
    mols = moltools.decompose(atoms, cutoff=cutoff)
    for mol in mols:
        mol = aux.strip_elements(mol, exclude_atoms)
        if len(mol) == 5:
            adj_mat = adjacency_matrix(mol, cutoff, diag=0)       
            G1 = nx.from_numpy_matrix(adj_mat)
            for node in G1:
                if len(G1.edges(node)) ==4:
                     return True
    return False



def find_rings(atoms, side_atoms=True, cutoff = 1.1):
    """ Check if molecule has a "ring",
        defined as a k=2 connectivity. 

    Args:
        param1 : ASE periodic molecule object.
        param2 (optional): if "attached" single atoms is included in the ring.
        param3 (optional): cutoff used to define covalent networks

    Returns:
        True / False (Bool)    """
        
    G = atoms2network(atoms, cutoff=cutoff)
    if nx.is_connected(G) == False:
        raise ValueError("Input object is not a connected molecule. Decompose atoms object first. ")

    rings = []
    for edges in list(nx.k_edge_components(G,2)):
        if len(edges) > 1:
            rings.append(edges)

    for ring in rings:
        print(ring)
        print(G)
        G1 = G.subgraph(ring)
        print(G1.nodes)
    
        print(G.nodes - G1.nodes)

def has_cage(atoms, connectivity=5, exclude_atoms=['H'], cutoff=1.1):
    """ Check if a crystal structure has a molecular "cage",
        here defined as a k=3 connectivity. 

    Args:
        param1 : ASE periodic atoms object, a molecule or
                molecular crystal.
        param2 (optional): List of atom types (chemical symbols)
                to exclude in the network analysis. By default 'H' (hydrogen)
                is excluded
        param3 (optional): cutoff used to define covalent networks

    Returns:
        True / False (Bool)
    """

    print('Not implemented')
    return False


def chain_lengths(atoms, exclude_atoms=['H'], node_edges=[2], cutoff=1.1):
    """ A "chain" is a term we use for a bridge in network theory
    (with molecules as nodes) where each node only has two edges
    (i.e. bonds to two atoms).

    Note: The method was to developed to automatically identify
          molecules that are likely to be non-rigid, in screening
          of databases, such as the Cambridge Crystallographic 
          database.

    Args:
        param1 : ASE periodic atoms object, a molecule or
                molecular crystal.
        param2 (optional): List of atom types (chemical symbols)
                to exclude in the network analysis. By default 'H' (hydrogen)
                is excluded
        param3 (optional): cutoff used to define covalent networks

    Returns:
        List of integers (chain lengths).
    """
    if isinstance(node_edges, int):
        node_edges = [node_edges]
    print(node_edges)
    mols = moltools.decompose(atoms, cutoff=cutoff)

    lens_chains = []
    for mol in mols:
        for symb in exclude_atoms:
            not_atom = np.array(mol.get_chemical_symbols()) != symb
            mol = mol[not_atom]
        adj_mat = adjacency_matrix(mol, cutoff, diag=0)
        G1 = nx.from_numpy_matrix(adj_mat)
        G_bridges = nx.Graph(nx.bridges(G1))
        for nodes_bridges in nx.connected_components(G_bridges):
            H = G_bridges.subgraph(nodes_bridges)
            nodes_vertex = []
            for node in nodes_bridges:
                if len(H.edges(node)) in node_edges:
                    nodes_vertex.append(node)
            Ichain = H.subgraph(nodes_vertex)
            for chains in nx.connected_components(Ichain):
                lens_chains.append(len(chains))

    return lens_chains
