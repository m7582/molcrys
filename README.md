# MOLCRYS

MOLCRYS is a collection of Python tools and modules developed for the purpose of manipuating and analysis of molecular crystals and is compatible with Atomic Simulation Environment [ASE](https://wiki.fysik.dtu.dk/ase/). 


## Installation

For instalation, you can directly clone the respository:

```bash
git clone https://gitlab.com/m7582/molcrys.git

```
Then you can add the respositoy directory to your Python path by adding this line to your Python script

```python
import sys
sys.path.append("/path/to/molcrys/package")
```


## Usage
Here is an example to produce interpolated images needed to calculate spontaneous polarizations in organic proton-transfer ferroelectrics (OPTFes).
starting from the ferroelectric phase gradually mixing the ferrolectric phase with the inverted phase:
0.0 mixing = ferroelctric phase
0.5 mixing = hypothetical symmetrized paraelectric phase
You can download the CIF file used in this example from [this link](https://www.ccdc.cam.ac.uk/structures/Search?Ccdcid=REZBOP&DatabaseToSearch=Published). The structure is 5,6-Dichloro-2-methyl-1H-benzimidazole wich is a known OPTFe. 

```python
import numpy as np
from ase.io import read, write
from molcrys import transform
import spglib


atoms = read('REZBOP.cif')

num_images = 6
image_number = 0

for frac in np.linspace(0, 0.5, num_images):
    # Interpolate the atoms using a linear interpolation
    interpolated_atoms = transform.proton_transfer(atoms, alpha=frac)
    print('interpolated atoms')

    # The space group of the last image is centrosymmetric
    write(f'POSCAR_{image_number}', interpolated_atoms, format='vasp')
    image_number += 1
    
```

## Contributing

For major changes, please open an issue first to discuss what you would like to change.

## Citation


## License

[MIT](https://choosealicense.com/licenses/mit/)
