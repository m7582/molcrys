import spglib
import numpy as np


def pseudo_inv_symmetry(crystal, symprec=0.5):
    """Tool to check pseuodo inversion symmetry of a molecular
    crystal, i.e. does approximative inversion symmetry arise if hydrogen
    atoms are neglected.
    Possibly an alternative to PLATON.

    Args:
        param1 : ASE periodic atoms object
        param2 (optional): symprec (used in spglib)

    Returns:
        True if (pseudo) inversion symmetry is present, False otherwise.
   """

    notH = np.array(crystal.get_chemical_symbols()) != 'H'
    crystal_bare = crystal[notH]
    spg1 = spglib.get_spacegroup(crystal)
    spg2 = spglib.get_spacegroup(crystal_bare, symprec=symprec)

    print('Spacegroup: %s' % spg1)
    print('Pseodo-spacegroup: %s' % spg2)
    if spg1 != spg2:
        print('Spacegroup changed')
    else:
        print('Spacegroup same')

    for rot in spglib.get_symmetry(crystal)['rotations']:
        if np.linalg.det(rot) < 0:
            print('Full inversion symmetry present')
            return True

    rot_bare = spglib.get_symmetry(crystal_bare, symprec)['rotations']

    for rot in rot_bare:
        if np.linalg.det(rot) < 0:
            print('Pseudo inversion symmetry present')
            return True

    return False
