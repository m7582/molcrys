import numpy as np
from molcrys import decompose, unwrap
from molcrys import metric
from scipy import optimize


def strip_elements(atoms, exclude_atoms=['H']):
    """Exclude atoms with specified chemical elements from atoms list.

    Args:
        atoms (ASE.Atoms): ASE atoms object.
        exclude_atoms (list[str], optional): List of species to ignore. Default ['H'].

    Returns:
        ASE.Atoms: ASE atoms object with fewer atoms.
    """
    chem = atoms.get_chemical_symbols()
    include_ind = np.isin(chem, exclude_atoms, invert=True)
    return atoms[include_ind]


def match_atoms(atoms1, atoms2, exclude_atoms=['H'], **kwargs):
    """Find the operation that makes two atoms objects with the same element maximize overlap.

    Args:
        atoms1 (ASE.Atoms): First ASE atoms object.
        atoms2 (ASE.Atoms): Second ASE atoms object.
        exclude_atoms (list[str], optional): Ignore species. Default ['H'].

    Returns:
        np.ndarray: Suggested translation vector.
    """
    atoms1 = unwrap(atoms1, join=True, **kwargs)
    atoms2 = unwrap(atoms2, join=True, **kwargs)

    if exclude_atoms:
        atoms1 = strip_elements(atoms1, exclude_atoms)
        atoms2 = strip_elements(atoms2, exclude_atoms)

    mols1 = decompose(atoms1, **kwargs)
    mols2 = decompose(atoms2, **kwargs)

    Ts = []

    for i, mol1 in enumerate(mols1):
        for j, mol2 in enumerate(mols2):
            if mol1.get_chemical_symbols() == mol2.get_chemical_symbols():
                M1 = mol1.get_center_of_mass()
                M2 = mol2.get_center_of_mass()
                tmol = mol2.copy()
                T = M1 - M2
                Ts.append(T)

    dists = []
    Tnew = []
    for Tguess in Ts:
        T, d = match_atoms_local(atoms1, atoms2, Tguess)
        Tnew.append(T)
        dists.append(d)
    return Tnew[np.argmin(dists)]


def match_atoms_local(atoms1, atoms2, Tguess):
    """Optimize translation vector to minimize atoms distance.

    Args:
        atoms1 (ASE.Atoms): First ASE atoms object.
        atoms2 (ASE.Atoms): Second ASE atoms object.
        Tguess (np.ndarray): Initial guess for translation vector.

    Returns:
        tuple: Optimized translation vector and corresponding atoms distance.
    """
    def func(T):
        atoms2_cp = atoms2.copy()
        atoms2_cp.translate(T)
        return metric.atoms_distance(atoms1, atoms2_cp)

    T = optimize.fmin(func, Tguess, ftol=0.01, xtol=0.005, maxiter=100)
    return T, func(T)

    

