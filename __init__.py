import numpy as np
from ase import Atoms
from scipy.sparse.csgraph import connected_components
import molcrys.metric as metric


def decompose(atoms, cutoff=1.3, wrapping=True, **kwargs):
    """
    Decompose an Atoms object into a list of connected atoms objects.
    
    Args:
        atoms (Atoms): An Atoms object.
        cutoff (float, optional): Cutoff distance for adjacency matrix. Defaults to 1.3.
        wrapping (bool, optional): Whether to wrap atoms into the unit cell. Defaults to True.
        **kwargs: Additional keyword arguments for adjacency_matrix function.

    Returns:
        list: A list of connected atoms objects.
    """
    adj_matrix = metric.adjacency_matrix(atoms, cutoff, wrapping=wrapping, **kwargs)
    num_mol, connected_indices = connected_components(adj_matrix, directed=False)
    list_of_molecules = []
    for n_molecule in range(num_mol):
        inds = list(np.where(connected_indices == n_molecule)[0])
        list_of_molecules.append(atoms[inds])
    return list_of_molecules


def is_connected(atoms, wrapping=True, **kwargs):
    """
    Check if an Atoms object is connected.
    
    Args:
        atoms (Atoms): An Atoms object.
        wrapping (bool, optional): Whether to wrap atoms into the unit cell. Defaults to True.
        **kwargs: Additional keyword arguments for decompose function.

    Returns:
        bool: True if the atoms object is connected, False otherwise.
    """
    parts = decompose(atoms, wrapping=wrapping, **kwargs)
    return len(parts) == 1


def move_within_unit_cell(molecules):
    """
    Move molecules to within the unit cell.
    
    Args:
        molecules (list): A list of molecules.

    Returns:
        list: A list of molecules moved within the unit cell.
    """
    cell = molecules[0].cell
    invcell = np.linalg.inv(cell)
    moved_molecules = []

    for mol in molecules:
        com = mol.get_center_of_mass()
        frac_coords = np.dot(invcell, com)
        diff_vec = -np.floor(frac_coords)
        T = np.dot(cell.T, diff_vec)
        mol.translate(T)
        moved_molecules.append(mol)

    return moved_molecules


def unwrap(atoms, join=True, **kwargs):
    """
    Unwrap connected molecules that are wrapped across the unit cell.

    Args:
        atoms (Atoms): An Atoms object.
        join (bool, optional): Whether to join the unwrapped molecules
            back into a single Atoms object. Defaults to True.

    Returns:
        Atoms or list: An Atoms object with unwrapped molecules if join is True,
            otherwise a list of unwrapped molecules.
    """
    molecules = decompose(atoms, wrapping=True, **kwargs)
    mol_list = []

    for molecule in molecules:
        parts = decompose(molecule, wrapping=False, **kwargs)
        parts = move_within_unit_cell(parts)
        base = parts[0]
        pieces = parts[1:]

        while len(pieces) > 0:
            np.random.shuffle(pieces)
            part = pieces[0]
            for ijk in metric.neighbours:
                vec = metric.nb_vector(ijk, base.cell)
                tpart = part.copy()
                tpart.translate(vec)
                new = base + tpart
                if is_connected(new, wrapping=False):
                    base = new
                    base = move_within_unit_cell([base])[0]
                    pieces.pop(0)
                    break

        mol_list.append(base)
    mol = mol_list[0]

    if join:
        atoms = mol_list[0]
        for mol in mol_list[1:]:
            atoms += mol
        return atoms
    else:
        return mol_list

